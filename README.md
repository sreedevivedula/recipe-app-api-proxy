# Recipe App API Proxy
NGINX proxy app for our recipe app API

## Usage

### Environment Variables

* `LISTEN_PORT` default:`8000`
* `APP_HOST` default: `app`
* `APP_PORT` default: `9000`